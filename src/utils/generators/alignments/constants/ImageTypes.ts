export const NUCLEOTIDE_ALIGNMENT = 'nucleotide';
export const NUCLEOTIDE_EXTENDED_ALIGNMENT = 'nucleotide-extended';
export const PROTEIN_ALIGNMENT = 'protein';
export const PROTEIN_EXTENDED_ALIGNMENT = 'protein-extended';

export type ImageType = typeof NUCLEOTIDE_ALIGNMENT | typeof NUCLEOTIDE_EXTENDED_ALIGNMENT  | typeof PROTEIN_ALIGNMENT | typeof PROTEIN_EXTENDED_ALIGNMENT;