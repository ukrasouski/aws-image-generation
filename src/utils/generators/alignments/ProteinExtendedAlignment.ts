import BaseExtendedAlignment from './BaseExtendedAlignment';
import { PROTEIN_EXTENDED_ALIGNMENT } from './constants/ImageTypes';

export default class AminoExtendedAlignment extends BaseExtendedAlignment {
  setImageType() {
    this.imageType = PROTEIN_EXTENDED_ALIGNMENT;
    return this;
  }
};