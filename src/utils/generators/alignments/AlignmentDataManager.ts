import { animalClasses, latinNames } from './constants';
import {
    ImageType,
    NUCLEOTIDE_ALIGNMENT,
    NUCLEOTIDE_EXTENDED_ALIGNMENT,
    PROTEIN_ALIGNMENT,
    PROTEIN_EXTENDED_ALIGNMENT
} from './constants/ImageTypes';
import { animalClasses as expandedAnimalClasses } from './constants/constants-extended';
import v2_evo_tree_path from './img/v2_evo_tree_path';
import v3_evo_tree_path from './img/v3_evo_tree_path';

export default class AlignmentDataManager {
    protected data: Record<string, any>;
    protected accessor: {
        alignmentKey: string; 
        replacement: string;
        alterationType: string;
    };
    protected imageType: ImageType;
    protected displaySequenceLength: number = 51;
    protected positionStart: number;
    protected positionEnd: number;
    protected _alignments: {
        seq_in_align: string
    }[];
    protected _patientAlignment: {
        seq_in_align: string
    };

    variantStart: number;
    variantLength: number;
    alignmentList: {}[];
    speciesList: string[];
    labels: {}[];

    constructor(alteration = {}, imageType: ImageType) {
      this.data = alteration;
      this.imageType = imageType;
      // The length of the sequence we are going to display in the image.
      // Only static right now
      this.setDataKeys();
      if (this.canRender()) {
        this.processData();
      }
    }
  
    setDataKeys() {
      const { imageType } = this;
      let alterationType, replacement, alignmentKey;
      if (imageType === PROTEIN_ALIGNMENT || imageType === PROTEIN_EXTENDED_ALIGNMENT) {
        replacement = 'replacement_amino';
        alterationType = 'protein_type';
        alignmentKey = imageType === PROTEIN_ALIGNMENT ? 'protein_alignment' : 'protein_alignment_v3';
      } else {
        replacement = 'replacement_nucleotide';
        alterationType = 'nucleotide_type';
        alignmentKey = imageType === NUCLEOTIDE_ALIGNMENT ? 'nucleotide_alignment' : 'nucleotide_alignment_v3';
      }
  
      this.accessor = { replacement, alterationType, alignmentKey };
  
      return this;
    }
  
    processData() {
      const {
        [this.alignmentKey]: { position_start, position_end, alignment, labels }
      } = this.data;
      this.positionStart = Number(position_start);
      this.positionEnd = Number(position_end);
  
      const start = this.getImageStartPosition();
  
      this.labels = labels.slice(start);
  
      this.variantStart = this.positionStart - start - 1;
      // If it's an insertion we make it 1 even if it is more
      // We don't display the inserted sequence in the image if it's more than 1 so it will ALWAYS be 1
      this.variantLength = this.isInsertion() ? 1 : position_end - position_start + 1;
  
      const species = alignment.map(
        ({ sp_name, sp_latin_name }: { sp_name: string, sp_latin_name: string }) => 
            (latinNames.indexOf(sp_name) === -1 ? sp_name : sp_latin_name)
      );
  
      this.speciesList = ['Patient'].concat(species);
  
      this.alignmentList = [this.patientAlignment()]
        .concat(this.alignments)
        .map(({ seq_in_align }) => seq_in_align.substring(start, start + this.displaySequenceLength));
  
      return this;
    }
  
    getImageStartPosition() {
      const variantMidPoint = Math.ceil((this.positionEnd - this.positionStart) / 2);
      const startingResidues = 26;
  
      // If the middle of the hilighted section is less than the middle of our image then we start at 0.
      if (this.positionStart + variantMidPoint < startingResidues) {
        return 0;
      }
  
      const sequenceLength = this.data[this.alignmentKey].length;
  
      if (this.positionEnd + startingResidues > sequenceLength) {
        return sequenceLength - this.displaySequenceLength;
      }
  
      return Math.abs(this.positionStart - startingResidues) + variantMidPoint;
    }
  
    patientAlignment() {
      if (this._patientAlignment) {
        return this._patientAlignment;
      }
  
      let seq_in_align;
      const { positionEnd, positionStart, alterationType } = this;
      const ref = this.humanAlignment.seq_in_align;
      // Make sure the replacement case is uniform
      const original = ref.substr(positionStart - 1, this.replacement.length);
      let replacement = '';
      for (let i = positionStart; i <= positionEnd; i++) {
        const originalValue = original[i - positionStart];
        const replacementValue = this.replacement[i - positionStart];
        if (originalValue && replacementValue) {
          replacement += this.uniformCase(originalValue, replacementValue);
        }
      }
  
      if (alterationType === 'substitution' || alterationType === 'del') {
        seq_in_align = `${ref.substr(0, positionStart - 1)}${replacement}${ref.substr(positionEnd)}`;
      } else {
        if (this.isInsertion()) {
          // Since we are inserting one more cell we need to start the variant one cell after
          this.variantStart++;
          this.labels.splice(Math.floor(this.displaySequenceLength / 2), 0, 'ins');
  
          // If the inserted replacement is more than one we need to show it in a legend
          // We just add a + here to represent there is a replacement
          seq_in_align = this.makeInsertionSequence(ref, replacement.length > 1 ? '+' : replacement);
        } else {
          replacement = this.isFrameshift() ? '+' : new Array(positionEnd - positionStart + 1).fill('-').join('');
          seq_in_align = `${ref.substr(0, positionStart - 1)}${replacement}${ref.substr(positionEnd)}`;
        }
      }
  
      return (this._patientAlignment = { seq_in_align });
    }
  
    makeInsertionSequence(seq: string, ins = '') {
      return `${seq.substr(0, this.positionStart)}${ins}${seq.substr(this.positionEnd - 1)}`;
    }
  
    canRender() {
      return !!this.data[this.alignmentKey] && !this.data[this.alignmentKey].error;
    }
  
    alignmentVersion() {
      return this.data.condensed_alignment_version;
    }
  
    isV2() {
      return this.alignmentVersion().toUpperCase() === 'V2';
    }
  
    isFrameshift() {
      return this.alterationType === 'fs';
    }
  
    isInsertion() {
      return this.alterationType === 'ins';
    }
  
    isExpandedAlignment() {
      return this.imageType === PROTEIN_EXTENDED_ALIGNMENT || this.imageType === NUCLEOTIDE_EXTENDED_ALIGNMENT;
    }
  
    uniformCase(letterToMatch: string, letterToChange: string) {
      return letterToMatch === letterToMatch.toLowerCase() ? letterToChange.toLowerCase() : letterToChange.toUpperCase();
    }
  
    get alignmentKey() {
      return this.accessor.alignmentKey;
    }
  
    get replacementKey() {
      return this.accessor.replacement;
    }
  
    get alterationTypeKey() {
      return this.accessor.alterationType;
    }
  
    get alterationType() {
      return this.data[this.alterationTypeKey];
    }
  
    get humanAlignment() {
      return (
        this.data[this.alignmentKey] &&
        Array.isArray(this.data[this.alignmentKey].alignment) &&
        this.data[this.alignmentKey].alignment[0]
      );
    }
  
    get alignments() {
      if (this._alignments) {
        return this._alignments;
      }
      let alignments = this.data[this.alignmentKey] && this.data[this.alignmentKey].alignment;
  
      if (Array.isArray(alignments) && this.isInsertion()) {
        // If this is an insertion we need a placeholder here for all other sequences
        alignments = alignments.map(({ seq_in_align }) => ({
          seq_in_align: this.makeInsertionSequence(seq_in_align, '-')
        }));
      }
      return (this._alignments = alignments);
    }
  
    get replacement() {
      return this.data[this.replacementKey];
    }
  
    get animalClasses() {
      return this.isExpandedAlignment() ? expandedAnimalClasses : animalClasses[this.alignmentVersion().toUpperCase()];
    }
  
    get evoTreePath() {
      return require(`./img/${this.alignmentVersion().toLowerCase()}_evo_tree_path`);
    }
  
    get evoTreeSettings() {
      return this.isV2() ? this.v2EvoTreeSettings() : this.v3EvoTreeSettings();
    }
  
    get variantTick() {
      // If this spans the entire display length then we just show the middle
      if (this.variantLength >= this.displaySequenceLength) {
        return Math.floor(this.displaySequenceLength / 2);
      }
      // If the alteration is longer than 2bp's we are going to put the label in the middle of the alteration
      return this.variantStart + (this.variantLength > 2 ? Math.floor(this.variantLength / 2) : 0);
    }
  
    v2EvoTreeSettings() {
      return {
        d: v2_evo_tree_path,
        transform: `scale(.505)translate(0,125)`
      };
    }
  
    v3EvoTreeSettings() {
      return {
        d: v3_evo_tree_path,
        transform: `scale(.505,.541)translate(0,115)`
      };
    }
  };
  