import {
  logger,
  createSvgResponse,
  getAlterationFromEvent,
  AlterationApi,
  createNaSvg,
  S3Client,
  scaleSvg
} from '@utils';
import BaseAlignment from '@utils/generators/alignments/BaseAlignment';
import { APIGatewayEvent, APIGatewayProxyResult } from 'aws-lambda';

const s3Client = new S3Client(process.env.BUCKET_NAME);

export default class AbstractHandler {
  protected generator: typeof BaseAlignment;
  protected folder: string;

  constructor(generator: typeof BaseAlignment, folder: string) {
    this.generator = generator;
    this.folder = folder;
  }

  createHandler() {
    return async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
      const alteration = getAlterationFromEvent(event);
      logger.info(`Processing alteration ${alteration}`);

      const scale = event?.queryStringParameters?.scale;
      logger.info(`Scale is ${scale}`);

      //check if image was already generated;
      const storedSvg = await s3Client.getFile(alteration, this.folder);
      if (storedSvg !== null) {
        logger.info('Using previously generated svg.');
        return createSvgResponse(storedSvg, 200);
      }

      try {
        //parse alteration first
        const { success, message, data } = await AlterationApi.parseAlteration<{}>(alteration);
        if (!success) {
          logger.error('Unable to fetch bio-api data: ' + message);
          return createSvgResponse(createNaSvg(message ?? 'Unknown Error'), 206);
        }
        const svg = new this.generator(data).render();
        await s3Client.upload(svg, alteration, this.folder);
        return createSvgResponse(scale ? scaleSvg(svg, parseFloat(scale)) : svg, 200);
      } catch (e) {
        return createSvgResponse(createNaSvg(e.message ?? 'Unknown Error'), 206);
      }
    }
  }
}