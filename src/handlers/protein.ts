import AbstractHandler from './AbstractHandler';
import ProteinAlignment from '@utils/generators/alignments/ProteinAlignment';

const FOLDER = 'protein';

export const handler = new AbstractHandler(ProteinAlignment, FOLDER).createHandler();